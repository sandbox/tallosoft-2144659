CONTENTS OF THIS FILE
---------------------
 * Overview
 * Features
 * Requirements
 * Known Problems
 * Similar Projects
 
 
OVERVIEW
--------
This module adds BluePay Secure Hosted Forms as a payment option for Drupal 
Commerce.

BluePay’s hosted payment forms allow merchants to accept and process credit 
cards online through a Web form hosted on BluePay’s secure server.

Allows merchants to:
	Accept convenient and secure online payments with a reduced risk of liability.
	Cut paper billing costs with online recurring payment capabilities.
	Reduce PCI scope through BluePay’s secure, PCI compliant databases.

If you have a BluePay account, all you will need to do is configure this payment
method with some of your account information, choose your transacation types, 
and the cards you can accept, and Secure Hosted Forms will take care of the 
rest!

FEATURES
--------
Integration to Bluepay hostedforms as a payment solution.

Requirements:
PHP5
Drupal Commerce

Note: This module requires you to have a BluePay account in order to allow users
to checkout and pay with Bluepay. Sign up at 
http://www.bluepay.com/get-started-today.

KNOWN PROBLEMS
--------------
Due to considerations around ACH and delays in ACH notifications, ACH has been
removed from this module currently, to be potentially added again in the
future.

SIMILAR PROJECTS
----------------
This project is similar to most payment options for Drupal Commerce.  The
primary difference is that it is specifically designed to work directly with
the Bluepay Secure Hosted forms solution, and is the only module that does so.

This project was sponsored by Bluepay.
